package ru.echodc.myfirstapplication.comments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.echodc.myfirstapplication.ApiUtils;
import ru.echodc.myfirstapplication.R;
import ru.echodc.myfirstapplication.model.Comment;

public class ShowDialogFragment extends DialogFragment {

  @BindView(R.id.dialog_tv_author)
  TextView mDialogTvAuthor;
  @BindView(R.id.dialog_tv_message)
  TextView mDialogTvMessage;

  public static final String COMMENT_ID = "COMMENT_ID";

  private Comment mComment;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    //    ButterKnife.bind(this, view);
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.dialog_show_comment, container, false);
    return v;
  }

  private void getComment() {
    ApiUtils.getApiService()
        .getCommentByIdFromServer(mComment.getId())
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(
            comment -> {
              mComment.getText();
              mDialogTvAuthor.setText(mComment.getAuthor());
            });
  }

  @NonNull
  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    if (getArguments() != null) {
      mComment = (Comment) getArguments().getSerializable(COMMENT_ID);
    } else {
      Toast.makeText(getContext(), "Данные о комментарии в диалог не добавились!", Toast.LENGTH_SHORT).show();
    }
    getComment();

    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    builder.setTitle("Комментарий пользователя");

    View linearlayout = getActivity().getLayoutInflater().inflate(R.layout.dialog_show_comment, null, false);
    mDialogTvAuthor = linearlayout.findViewById(R.id.dialog_tv_author);
    mDialogTvMessage = linearlayout.findViewById(R.id.dialog_tv_message);
    mDialogTvAuthor.setText(mComment.getAuthor());
    mDialogTvMessage.setText(mComment.getText());
    builder.setView(linearlayout);

    //    Кнопки
    builder.setPositiveButton("Редактировать", (dialogInterface, i) -> {
          // EditDialog
          Toast.makeText(getContext(), "Скоро появится диалог редактирования комментария", Toast.LENGTH_SHORT).show();
        }
    );
    builder.setNegativeButton("Отмена",
        (dialogInterface, i) -> dialogInterface.cancel()
    );
    builder.setOnCancelListener(dialogInterface -> dialogInterface.dismiss());

    return builder.create();
  }
}
