package ru.echodc.myfirstapplication.comments;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;
import ru.echodc.myfirstapplication.R;
import ru.echodc.myfirstapplication.model.Comment;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsHolder> {

  @NonNull
  private final List<Comment> mComments = new ArrayList<>();
  private final OnItemClickListener mOnClickListener;

  public CommentsAdapter(OnItemClickListener onClickListener) {
    mOnClickListener = onClickListener;
  }

  @Override
  public CommentsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    View view = inflater.inflate(R.layout.list_item_comment, parent, false);
    return new CommentsHolder(view);
  }

  @Override
  public void onBindViewHolder(CommentsHolder holder, int position) {
    Comment comment = mComments.get(position);
    holder.bind(comment, mOnClickListener);
  }

  @Override
  public int getItemCount() {
    return mComments.size();
  }

  public interface OnItemClickListener {

    void onItemClick(Comment comment);
  }

  public void addData(List<Comment> data, boolean isRefreshed) {
    if (isRefreshed) {
      mComments.clear();
    }
    //    mComments.add(mAlbum.getId(), (Comment) data);
    mComments.addAll(data);

    notifyDataSetChanged();
  }


  @Override
  public long getItemId(int position) {
    return super.getItemId(position);
  }

  public List<Comment> getComments(){ return mComments;}
}
