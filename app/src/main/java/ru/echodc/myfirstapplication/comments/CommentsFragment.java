package ru.echodc.myfirstapplication.comments;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import java.util.List;
import ru.echodc.myfirstapplication.ApiUtils;
import ru.echodc.myfirstapplication.App;
import ru.echodc.myfirstapplication.NetworkStatusChecker;
import ru.echodc.myfirstapplication.R;
import ru.echodc.myfirstapplication.db.MusicDao;
import ru.echodc.myfirstapplication.model.Album;
import ru.echodc.myfirstapplication.model.Comment;

public class CommentsFragment extends Fragment implements OnRefreshListener, OnKeyListener {


  public static final String ALBUM_ID = "ALBUM_ID";
  public static final String COMMENT_ID = "COMMENT_ID";
  private static final String TAG = CommentsFragment.class.getSimpleName();

  @BindView(R.id.recycler)
  RecyclerView mRecyclerView;
  @BindView(R.id.refresher)
  SwipeRefreshLayout mRefresher;
  @BindView(R.id.errorView)
  View mErrorView;
  @Nullable
  @BindView(R.id.noElementView)
  View mNoElementView;

  @BindView(R.id.send_comment_view)
  View mSendCommentView;

  @BindView(R.id.et_comment)
  EditText mEtComment;
  @BindView(R.id.fab_send)
  FloatingActionButton mFabSend;

  private Album mAlbum;
  private boolean wasOnce = false;

  private boolean loadFromDB = false;
  private int commentsListSize = -1;

  public static CommentsFragment newInstance(Album album) {
    Bundle args = new Bundle();
    args.putSerializable(ALBUM_ID, album);

    args.putAll(args);

    CommentsFragment fragment = new CommentsFragment();
    fragment.setArguments(args);

    return fragment;
  }


  @NonNull
  private final CommentsAdapter mCommentsAdapter = new CommentsAdapter(
      comment -> {

        Bundle args = new Bundle();
        args.putSerializable(COMMENT_ID, comment);

        ShowDialogFragment dialogFragment = new ShowDialogFragment();
        dialogFragment.setArguments(args);

        //Аргументы передаются нормально
        dialogFragment.show(getFragmentManager().beginTransaction(), ShowDialogFragment.class.getSimpleName());
      });


  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fr_recycler, container, false);
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    ButterKnife.bind(this, view);

    mRefresher.setOnRefreshListener(this);
    mEtComment.setOnKeyListener(this);

  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);

    setRetainInstance(true);

    mAlbum = (Album) getArguments().getSerializable(ALBUM_ID);

    String commentTitle = getActivity().getResources().getString(R.string.comments_title);

    getActivity().setTitle(mAlbum.getName() + " / " + commentTitle);

    mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    mRecyclerView.setAdapter(mCommentsAdapter);

    onRefresh();

  }

  @Override
  public void onRefresh() {
    mRefresher.post(() -> {
      mRefresher.setRefreshing(true);
      getCommentByAlbumId();
    });
  }


  @SuppressLint("CheckResult")
  private void getCommentByAlbumId() {
    ApiUtils.getApiService()
        .getCommentsFromServerByAlbumId(mAlbum.getId())
        .subscribeOn(Schedulers.io())
        .doOnSuccess(
            comments -> {
              setLoadFromDB(false);
              getMusicDao().insertComments(comments);
            })
        .onErrorReturn(throwable -> {

          if (!NetworkStatusChecker.isNetworkAviable(getActivity())) {
            Log.d(TAG, "CommentsFragment: " + " Нет сети!");
            setLoadFromDB(true);
            return getMusicDao().getAlbumComments(mAlbum.getId());
          } else {
            return null;
          }
        })
        //        .doOnError(throwable -> showMessage(R.string.error_network_error))
        .observeOn(AndroidSchedulers.mainThread())
        .doOnSubscribe(disposable -> mRefresher.setRefreshing(true))
        .doFinally(() -> mRefresher.setRefreshing(false))
        .subscribe((List<Comment> comments) -> {
          if (getLoadFromDB()) {
            Snackbar.make(getView(), R.string.comment_load_from_db,4000).show();
          }
          if (!comments.isEmpty()) {
            mNoElementView.setVisibility(View.GONE);
            mErrorView.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            mSendCommentView.setVisibility(View.VISIBLE);
            mCommentsAdapter.addData(comments, true);
            if (commentsListSize != 1) {
              if (commentsListSize != comments.size()) {
                showMessage(R.string.comment_refresh);
              } else {
                showMessage(R.string.comment_not_new_comment);
              }
              commentsListSize = comments.size();
            }
          } else {
            mNoElementView.setVisibility(View.VISIBLE);
            mErrorView.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.GONE);
            mSendCommentView.setVisibility(View.VISIBLE);
            showMessage(R.string.comment_no_comment_on_the_server);
            //            mCommentsAdapter.addData(comments, true);
          }
        }, throwable -> {
          mErrorView.setVisibility(View.VISIBLE);
          mNoElementView.setVisibility(View.GONE);
          mRecyclerView.setVisibility(View.GONE);
          mSendCommentView.setVisibility(View.GONE);
        });
  }

  private MusicDao getMusicDao() {
    return ((App) getActivity().getApplication()).getDatabase().getMusicDao();
  }

  @OnClick(R.id.fab_send)
  public void sendCommentClick() {
    Comment comment = new Comment(
        mAlbum.getId(),
        mEtComment.getText().toString());

    if (TextUtils.isEmpty(mEtComment.getText())) {
      showMessage(R.string.comment_send_comment_empty);
    } else {
      ApiUtils.getApiService()
          .sendCommentToServer(comment)
          .subscribeOn(Schedulers.io())
          .observeOn(AndroidSchedulers.mainThread())
          .doOnSubscribe(disposable -> mRefresher.setRefreshing(true))
          .doFinally(() -> mRefresher.setRefreshing(false))
          .subscribe(() -> {
            showMessage(R.string.comment_send_comment_ok);
            onRefresh();
            mCommentsAdapter.notifyDataSetChanged();
            //            mRecyclerView.scrollToPosition(mCommentsAdapter.getItemCount() - 1);
          }, throwable ->
              showMessage(R.string.comment_send_comment_error));
    }
  }

  private void showMessage(@StringRes int string) {
    Activity activity = getActivity();
    if (activity != null) {
      Toast.makeText(getActivity(), string, Toast.LENGTH_SHORT).show();
    }
  }

  @Override
  public boolean onKey(View view, int actionId, KeyEvent keyEvent) {
    if (actionId == KeyEvent.KEYCODE_ENTER) {
      sendCommentClick();
      return true;
    }
    return false;
  }

  private void showEditCommentDialog(int position) {
    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
    dialogBuilder.setTitle("Редактирование комментария")
        .setMessage((int) mCommentsAdapter.getItemId(position))
        .setPositiveButton("Ok", (dialog, i) -> mCommentsAdapter.getItemId(position))
        .setNegativeButton("Отмена", (dialog, i) -> dialog.cancel())
        .setOnCancelListener(dialogInterface -> mCommentsAdapter.notifyDataSetChanged())
        .show();
  }

  private boolean isDataChanged(List<Comment> comments) {
    List<Comment> list = mCommentsAdapter.getComments();

    return !comments.equals(list);
  }

  private void setLoadFromDB(boolean status) {
    this.loadFromDB = status;
  }

  private boolean getLoadFromDB() {
    return loadFromDB;
  }

}
