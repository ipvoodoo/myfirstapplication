package ru.echodc.myfirstapplication.comments;

import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import ru.echodc.myfirstapplication.R;
import ru.echodc.myfirstapplication.model.Comment;

public class CommentsHolder extends RecyclerView.ViewHolder {

  public static final String TAG = CommentsHolder.class.getSimpleName();

  @BindView(R.id.tv_comment_id)
  TextView mTvCommentId;

  @BindView(R.id.tv_comment_user_name)
  TextView mTvCommentUserName;
  @BindView(R.id.tv_comment_timestamp)
  TextView mTvCommentTimestamp;
  @BindView(R.id.tv_comment)
  TextView mTvComment;

  public CommentsHolder(View itemView) {
    super(itemView);
    ButterKnife.bind(this, itemView);
  }

  public void bind(Comment comment, CommentsAdapter.OnItemClickListener onItemClickListener) {
    mTvCommentId.setText(String.valueOf(comment.getId()));
    mTvCommentUserName.setText(comment.getAuthor());
    //    mTvCommentTimestamp.setText(convertTimeStampToNormalDate(comment.getTimestamp()));
    normalTime(comment.getTimestamp());
    mTvComment.setText(comment.getText());
    if (onItemClickListener != null) {
      itemView.setOnClickListener(view -> onItemClickListener.onItemClick(comment));
    }
  }

  private void normalTime(String inDate) {

    String timestamp = inDate;
    SimpleDateFormat writeFormat = new SimpleDateFormat();
    String stringDate = null;
    if (timestamp != null) {
      try {
        Date commentDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US).parse(timestamp);

        stringDate = "";
        if (DateUtils.isToday(commentDate.getTime())) {

          writeFormat = new SimpleDateFormat("HH:mm", Locale.US);
          stringDate = writeFormat.format(commentDate);
        } else {
          writeFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.US);
          stringDate = writeFormat.format(commentDate);
        }
        mTvCommentTimestamp.setText(stringDate);
      } catch (ParseException e) {
        e.printStackTrace();
      }
    }
  }
}
