package ru.echodc.myfirstapplication;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import ru.echodc.myfirstapplication.model.User;

public class RegistrationFragment extends Fragment {

  public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

  @BindView(R.id.etLogin)
  EditText mEmail;
  @BindView(R.id.etName)
  EditText mName;
  @BindView(R.id.etPassword)
  EditText mPassword;
  @BindView(R.id.etPasswordAgain)
  EditText mPasswordAgain;
  @BindView(R.id.btnRegistration)
  Button btnRegistration;
  @BindView(R.id.cvRegistration)
  CardView mCvRegistration;

  private Animation mShake;


  public static RegistrationFragment newInstance() {
    return new RegistrationFragment();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {

    View view = inflater.inflate(R.layout.fr_registration, container, false);

    ButterKnife.bind(this, view);

    return view;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    initErrorAnimation();
  }

  private boolean isInputValid() {

    String email = mEmail.getText().toString();
    String name = mName.getText().toString();

    String password = mPassword.getText().toString();
    String passwordAgain = mPasswordAgain.getText().toString();

    if (!isEmailValid(email) && !isNameValid(name) && !isPasswordValid()) {
      return true;
    }

    if (!isEmailValid(email)) {
      mEmail.requestFocus();
      mEmail.setError("Проверьте правильность данных!");
      mEmail.startAnimation(mShake);
      return true;
    }

    if (!isNameValid(name) & isEmailValid(email)) {
      mEmail.clearFocus();
      mName.requestFocus();
      mName.findFocus().animate();
      mName.setError("Проверьте правильность данных!");
      mName.startAnimation(mShake);
      return true;
    }

    if (!isPasswordValid() & isEmailValid(email) & isNameValid(name)) {
      mName.clearFocus();
      mPassword.requestFocus();
      mPassword.setError("Проверьте правильность данных!");
      mPasswordAgain.setError("Проверьте правильность данных!");
      mPassword.startAnimation(mShake);
      mPasswordAgain.startAnimation(mShake);
      return true;
    }

    return false;
  }

  private boolean isEmailValid(String email) {
    return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
  }

  private boolean isNameValid(String name) {

    return !TextUtils.isEmpty(name);
  }

  public boolean isPasswordValid() {
    String password = mPassword.getText().toString();
    String passwordAgain = mPasswordAgain.getText().toString();

    return password.equals(passwordAgain);
    //        || !TextUtils.isEmpty(password)
    //        || !TextUtils.isEmpty(passwordAgain);
  }

  private void showMessage(@StringRes int message) {
    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
  }

  private void showMessage(String message) {
    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
  }


  @OnClick(R.id.btnRegistration)
  public void registrationClick() {
    if (isPasswordValid()) {
      mPassword.setTextColor(getActivity().getApplicationContext().getResources().getColor(R.color.invalidFormField));
      mPasswordAgain.setTextColor(getActivity().getApplicationContext().getResources().getColor(R.color.invalidFormField));

      User user = new User(
          mEmail.getText().toString(),
          mName.getText().toString(),
          mPassword.getText().toString());

      //      Для асинхронной работы вызываем метод enqueue(...)
      ApiUtils.getApiService()
          .registration(user)
          .subscribeOn(Schedulers.io())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe(() -> {
            showMessage(R.string.registration_success);
            getFragmentManager().popBackStack();


          }, throwable -> showMessage(R.string.request_error));
      //          .enqueue(
      //          new Callback<Void>() {
      //            //используем Handler, чтобы показывать ошибки в Main потоке, т.к. наши коллбеки возвращаются в рабочем потоке
      //            Handler mainHandler = new Handler(getActivity().getMainLooper());
      //
      //            @Override
      //            public void onResponse(Call<Void> call, final Response<Void> response) {
      //              mainHandler.post(new Runnable() {
      //                @Override
      //                public void run() {
      //                  if (!response.isSuccessful()) {
      //
      //                    Gson gson = new Gson();
      //                    JsonObject json = gson.fromJson(response.errorBody().charStream(), JsonObject.class);
      //                    Errors errors = gson.fromJson(json.get("errors"), Errors.class);
      //
      //                    if (errors.getEmail() != null) {
      //                      mEmail.requestFocus();
      //                      mEmail.setError(errors.getEmail().get(0));
      //                      mEmail.setTextColor(getActivity().getApplicationContext().getResources().getColor(R.color.invalidFormField));
      //                      mEmail.setAnimation(mShake);
      //
      //                    } else {
      //                      mEmail.clearFocus();
      //                      mEmail.setBackgroundColor(getActivity().getApplicationContext().getResources().getColor(R.color.validFormField));
      //                      mEmail.setTextColor(getActivity().getApplicationContext().getResources().getColor(R.color.primaryText));
      //                    }
      //                    if (errors.getName() != null) {
      //                      mName.setError(errors.getName().get(0));
      //                      mName.setTextColor(getActivity().getApplicationContext().getResources().getColor(R.color.invalidFormField));
      //                      mName.setAnimation(mShake);
      //                    } else {
      //                      mName.setBackgroundColor(getActivity().getApplicationContext().getResources().getColor(R.color.validFormField));
      //                      mName.setTextColor(getActivity().getApplicationContext().getResources().getColor(R.color.primaryText));
      //                    }
      //                    if (errors.getPassword() != null) {
      //                      mPassword.setError(errors.getPassword().get(0));
      //                      mPassword.setTextColor(getActivity().getApplicationContext().getResources().getColor(R.color.invalidFormField));
      //                      mPasswordAgain.setTextColor(getActivity().getApplicationContext().getResources().getColor(R.color.invalidFormField));
      //                      mPassword.setAnimation(mShake);
      //                      mPasswordAgain.setAnimation(mShake);
      //                    } else {
      //                      mPassword.setTextColor(getActivity().getApplicationContext().getResources().getColor(R.color.primaryText));
      //                      mPasswordAgain.setTextColor(getActivity().getApplicationContext().getResources().getColor(R.color.primaryText));
      //                      mPassword.setBackgroundColor(getActivity().getApplicationContext().getResources().getColor(R.color.validFormField));
      //                      mPasswordAgain.setBackgroundColor(getActivity().getApplicationContext().getResources().getColor(R.color.validFormField));
      //                    }
      //                    showError(response.code());
      //                  } else {
      //
      //                  }
      //                }
      //              });
      //            }
      //
      //            @Override
      //            public void onFailure(Call<Void> call, Throwable t) {
      //              mainHandler.post(new Runnable() {
      //                @Override
      //                public void run() {
      //
      //                }
      //              });
      //            }
      //          });
    } else {
      mPassword.setTextColor(getActivity().getApplicationContext().getResources().getColor(R.color.invalidFormField));
      mPasswordAgain.setTextColor(getActivity().getApplicationContext().getResources().getColor(R.color.invalidFormField));
      showMessage(R.string.input_error);
      mCvRegistration.startAnimation(mShake);
    }
  }

  // region ===================== Animation =====================
  private void initErrorAnimation() {
    mShake = new AnimationUtils().loadAnimation(getActivity(), R.anim.shake);
  }
  // endregion Animation

  // region ===================== Errors =====================

  private void showError(int code) {
    @StringRes int message;
    switch (code) {
      case 400:
        message = R.string.response_code_400;
        break;
      case 204:
        message = R.string.response_code_204;
        break;
      case 404:
        message = R.string.response_code_404;
        break;
      case 401:
        message = R.string.auth_error;
        break;
      case 500:
        message = R.string.response_code_500;
        break;
      default:
        message = R.string.registration_error;
    }
    showMessage(message);
  }
  // endregion Errors
}