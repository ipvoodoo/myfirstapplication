package ru.echodc.myfirstapplication.album;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.echodc.myfirstapplication.ApiUtils;
import ru.echodc.myfirstapplication.App;
import ru.echodc.myfirstapplication.R;
import ru.echodc.myfirstapplication.comments.CommentsFragment;
import ru.echodc.myfirstapplication.db.MusicDao;
import ru.echodc.myfirstapplication.model.Album;
import ru.echodc.myfirstapplication.model.Comment;
import ru.echodc.myfirstapplication.song.SongFragment;
import ru.echodc.myfirstapplication.song.SongsAdapter;

public class DetailAlbumFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

  public static final String ALBUM_KEY = "ALBUM_KEY";
  private static final String TAG = DetailAlbumFragment.class.getSimpleName();

  @BindView(R.id.recycler)
  RecyclerView mRecyclerView;
  @BindView(R.id.refresher)
  SwipeRefreshLayout mRefresher;
  @BindView(R.id.errorView)
  View mErrorView;


  //  private RecyclerView mRecyclerView;
  //  private SwipeRefreshLayout mRefresher;
  //  private View mErrorView;
  private Album mAlbum;
  private Comment mComment;

  //  private SongsAdapter mSongsAdapter;

  private Throwable mThrowable;

  private Boolean checkNetworkConnection() {
    mThrowable = new Throwable();

    if (ApiUtils.NETWORK_EXCEPTIONS.contains(mThrowable.getClass())) {
      return true;
    }
    return false;
  }

  @NonNull
  private final SongsAdapter mSongsAdapter = new SongsAdapter(
      song -> getFragmentManager().beginTransaction()
          .replace(R.id.fragmentContainer, SongFragment.newInstance(song))
          .addToBackStack(SongFragment.class.getSimpleName())
          .commit());

  public static DetailAlbumFragment newInstance(Album album) {
    Bundle args = new Bundle();
    args.putSerializable(ALBUM_KEY, album);

    DetailAlbumFragment fragment = new DetailAlbumFragment();
    fragment.setArguments(args);

    return fragment;
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    //    checkNetworkConnection();

    setHasOptionsMenu(true);
    //    checkNetworkConnection();
    Log.i(TAG, "onCreate: ");
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    Log.i(TAG, "onCreateView: ");
    return inflater.inflate(R.layout.fr_recycler, container, false);
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    ButterKnife.bind(this, view);

    mRefresher.setOnRefreshListener(this);

    Log.i(TAG, "onViewCreated: ");
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);

    mAlbum = (Album) getArguments().getSerializable(ALBUM_KEY);

    getActivity().setTitle(mAlbum.getName());

    mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    mRecyclerView.setAdapter(mSongsAdapter);

    onRefresh();

    Log.i(TAG, "onActivityCreated: ");
  }

  // region ===================== Menu =====================
  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    MenuInflater menuInflater = getActivity().getMenuInflater();
    menuInflater.inflate(R.menu.comment_menu, menu);

    super.onCreateOptionsMenu(menu, inflater);
    Log.i(TAG, "onCreateOptionsMenu: ");
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.actionViewComment:
        if (checkNetworkConnection()) {
          showMessage(R.string.error_network_error);
        }

        getActivity().getSupportFragmentManager().beginTransaction()
            .replace(R.id.fragmentContainer, CommentsFragment.newInstance(mAlbum))
            .addToBackStack(CommentsFragment.class.getSimpleName())
            .commit();
        break;
      default:
        break;
    }
    Log.i(TAG, "onOptionsItemSelected: ");
    return super.onOptionsItemSelected(item);
  }
  // endregion Menu

  @Override
  public void onRefresh() {
    mRefresher.post(() -> {
      mRefresher.setRefreshing(true);
      getAlbum();
    });
    Log.i(TAG, "onRefresh: ");
  }

  @SuppressLint("CheckResult")
  private void getAlbum() {
    ApiUtils.getApiService()
        .getAlbum(mAlbum.getId())
        .subscribeOn(Schedulers.io())
        .doOnSuccess(album -> getMusicDao().insertAlbum(album))
        //        .doOnSuccess(album -> getMusicDao().insertSongs(album.getSongs()))

        .onErrorReturn(throwable -> {
          if (ApiUtils.NETWORK_EXCEPTIONS.contains(throwable.getClass())) {
            //            mAlbum = getMusicDao().getAlbumWithId(mAlbum.getId());
            //            List<Song> songs = getMusicDao().getSongsFromAlbum(mAlbum.getId());
            //            mAlbum.setSongs(songs);

            Log.d(TAG, "getAlbumWithId: " + " Нет сети!");
            return getMusicDao().getAlbumWithId(mAlbum.getId());
          } else {
            return null;
          }
        })
        .doOnError(throwable -> showMessage(R.string.error_network_error))
        .observeOn(AndroidSchedulers.mainThread())
        .doOnSubscribe(disposable -> mRefresher.setRefreshing(true))
        .doFinally(() -> mRefresher.setRefreshing(false))
        .subscribe(album -> {
          mErrorView.setVisibility(View.GONE);
          mRecyclerView.setVisibility(View.VISIBLE);
          mSongsAdapter.addData(album.getSongs(), true);

        }, throwable -> {
          if (mRefresher.isRefreshing()) {
            mErrorView.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            mSongsAdapter.addData(getMusicDao().getSongsFromAlbum(mAlbum.getId()), true);
            showMessage(R.string.error_network_error);
          } else {
            mErrorView.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
          }
        });
    Log.i(TAG, "getAlbumWithId: ");
  }

  private MusicDao getMusicDao() {
    Log.i(TAG, "getMusicDao: ");
    return ((App) getActivity().getApplication()).getDatabase().getMusicDao();
  }

  private void showMessage(@StringRes int string) {
    Log.i(TAG, "showMessage: ");
    Toast.makeText(getActivity(), string, Toast.LENGTH_SHORT).show();
  }

}
