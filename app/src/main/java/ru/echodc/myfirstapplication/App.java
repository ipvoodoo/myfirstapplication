package ru.echodc.myfirstapplication;

import android.app.Application;
import android.arch.persistence.room.Room;
import ru.echodc.myfirstapplication.db.DataBase;

public class App extends Application {
  private DataBase mDatabase;

  @Override
  public void onCreate() {
    super.onCreate();

    mDatabase = Room.databaseBuilder(getApplicationContext(), DataBase.class, "music_database")
        .allowMainThreadQueries()
        .fallbackToDestructiveMigration()
        .build();
  }

  public DataBase getDatabase() {
    return mDatabase;
  }
}
