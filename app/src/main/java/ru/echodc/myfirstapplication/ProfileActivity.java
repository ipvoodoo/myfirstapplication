package ru.echodc.myfirstapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.echodc.myfirstapplication.model.User;

public class ProfileActivity extends AppCompatActivity {

  public static final String USER_KEY = "USER_KEY";
  public static final String TAG = ProfileActivity.class.getSimpleName();

  @BindView(R.id.tvEmail)
  TextView mEmail;
  @BindView(R.id.tvName)
  TextView mName;
  private User mUser;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.ac_profile);

    ButterKnife.bind(this);

    Bundle bundle = getIntent().getExtras();

    mUser = (User) bundle.get(USER_KEY);

    mEmail.setText(mUser.getEmail());
    mName.setText(mUser.getName());

    Log.d(TAG, "onCreate: " + mUser.getEmail());
    Log.d(TAG, "onCreate: " + mUser.getName());
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater menuInflater = getMenuInflater();
    menuInflater.inflate(R.menu.profile_menu, menu);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.actionLogout:
        startActivity(new Intent(this, AuthActivity.class));
        finish();
        break;
      default:
        break;
    }
    return super.onOptionsItemSelected(item);
  }
}
