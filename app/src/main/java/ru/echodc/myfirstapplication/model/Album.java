package ru.echodc.myfirstapplication.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;
import ru.echodc.myfirstapplication.model.converter.AlbumConverter;

@Entity // обозначаем таблицу
public class Album implements Serializable {

  @PrimaryKey
  @ColumnInfo(name = "id")// Обозначаем столбец, задаем имя
  @SerializedName("id")
  private int mId;
  @ColumnInfo(name = "name")
  @SerializedName("name")
  private String mName;
  @ColumnInfo(name = "release_date")
  @SerializedName("release_date")
  private String mReleaseDate;
  @SerializedName("songs")
//  @Ignore
  private List<Song> mSongs;

  public Album(int id, String name, String releaseDate) {
    mId = id;
    mName = name;
    mReleaseDate = releaseDate;
  }

  public int getId() {
    return mId;
  }

  public void setId(int id) {
    mId = id;
  }

  public String getName() {
    return mName;
  }

  public void setName(String name) {
    mName = name;
  }

  public String getReleaseDate() {
    return mReleaseDate;
  }

  public void setReleaseDate(String releaseDate) {
    mReleaseDate = releaseDate;
  }

  @TypeConverters(AlbumConverter.class)
  public List<Song> getSongs() {
    return mSongs;
  }
  @TypeConverters(AlbumConverter.class)
  public void setSongs(List<Song> songs) {
    mSongs = songs;
  }
}

