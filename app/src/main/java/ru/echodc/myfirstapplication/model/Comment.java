package ru.echodc.myfirstapplication.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

@Entity
public class Comment implements Serializable {

  @SerializedName("first")
  private String mFirst;
  @SerializedName("last")
  private String mLast;
  @SerializedName("prev")
  private String mPrev;
  @SerializedName("next")
  private String mNext;

  public String getFirst() {
    return mFirst;
  }

  public void setFirst(String first) {
    mFirst = first;
  }

  public String getLast() {
    return mLast;
  }

  public void setLast(String last) {
    mLast = last;
  }

  public String getPrev() {
    return mPrev;
  }

  public void setPrev(String prev) {
    mPrev = prev;
  }

  public String getNext() {
    return mNext;
  }

  public void setNext(String next) {
    mNext = next;
  }


  @SerializedName("path")
  private String mPath;
  @SerializedName("current_page")
  private int mCurrentPage;
  @SerializedName("per_page")
  private int mPerPage;
  @SerializedName("total")
  private int mTotal;
  @SerializedName("from")
  private int mFrom;
  @SerializedName("to")
  private int mTo;
  @SerializedName("last_page")
  private int mLastPage;

  public String getPath() {
    return mPath;
  }

  public void setPath(String path) {
    mPath = path;
  }

  public int getCurrentPage() {
    return mCurrentPage;
  }

  public void setCurrentPage(int currentPage) {
    mCurrentPage = currentPage;
  }

  public int getPerPage() {
    return mPerPage;
  }

  public void setPerPage(int perPage) {
    mPerPage = perPage;
  }

  public int getTotal() {
    return mTotal;
  }

  public void setTotal(int total) {
    mTotal = total;
  }

  public int getFrom() {
    return mFrom;
  }

  public void setFrom(int from) {
    mFrom = from;
  }

  public int getTo() {
    return mTo;
  }

  public void setTo(int to) {
    mTo = to;
  }

  public int getLastPage() {
    return mLastPage;
  }

  public void setLastPage(int lastPage) {
    mLastPage = lastPage;
  }

  @PrimaryKey
  @ColumnInfo(name = "id")
  @SerializedName("id")
  private int mId;
  @ColumnInfo(name = "album_id")
  @SerializedName("album_id")
  private int mAlbumId;
  @ColumnInfo(name = "text")
  @SerializedName("text")
  private String mText;
  @ColumnInfo(name = "author")
  @SerializedName("author")
  private String mAuthor;
  @ColumnInfo(name = "timestamp")
  @SerializedName("timestamp")
  private String mTimestamp;

//  public Comment(int id, int albumId, String text, String author, String timestamp) {
//    mId = id;
//    mAlbumId = albumId;
//    mText = text;
//    mAuthor = author;
//    mTimestamp = timestamp;
//  }

  public Comment(int albumId, String text) {
    mAlbumId = albumId;
    mText = text;
  }

  public int getId() {
    return mId;
  }

  public void setId(int id) {
    mId = id;
  }

  public int getAlbumId() {
    return mAlbumId;
  }

  public void setAlbumId(int albumId) {
    mAlbumId = albumId;
  }

  public String getText() {
    return mText;
  }

  public void setText(String text) {
    mText = text;
  }

  public String getAuthor() {
    return mAuthor;
  }

  public void setAuthor(String author) {
    mAuthor = author;
  }

  public String getTimestamp() {
    return mTimestamp;
  }

  public void setTimestamp(String timestamp) {
    mTimestamp = timestamp;
  }

}
