package ru.echodc.myfirstapplication.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

@Entity // обозначаем таблицу
public class Song implements Serializable {

  @PrimaryKey
  @ColumnInfo(name = "id")// Обозначаем столбец, задаем имя
  @SerializedName("id")
  private int mId;
  @ColumnInfo(name = "name")
  @SerializedName("name")
  private String mName;
  @ColumnInfo(name = "duration")
  @SerializedName("duration")
  private String mDuration;

  public Song(int id, String name, String duration) {
    mId = id;
    mName = name;
    mDuration = duration;
  }

  public int getId() {
    return mId;
  }

  public void setId(int id) {
    mId = id;
  }

  public String getName() {
    return mName;
  }

  public void setName(String name) {
    mName = name;
  }

  public String getDuration() {
    return mDuration;
  }

  public void setDuration(String duration) {
    mDuration = duration;
  }
}
