package ru.echodc.myfirstapplication.model.converter;

import android.arch.persistence.room.TypeConverter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;
import ru.echodc.myfirstapplication.model.Song;

public class AlbumConverter implements Serializable {
  @TypeConverter
  public String fromSongList(List<Song> songString) {
    if (songString == null) {
      return (null);
    }
    Gson gson = new Gson();
    Type type = new TypeToken<List<Song>>() {
    }.getType();
    String json = gson.toJson(songString, type);
    return json;
  }

  @TypeConverter
  public List<Song> toSongList(String songString) {
    if (songString == null) {
      return (null);
    }
    Gson gson = new Gson();
    Type type = new TypeToken<List<Song>>() {}.getType();
    List<Song> countryLangList = gson.fromJson(songString, type);
    return countryLangList;
  }
}
