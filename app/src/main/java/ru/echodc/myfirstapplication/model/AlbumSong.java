package ru.echodc.myfirstapplication.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

@Entity(foreignKeys = {
    @ForeignKey(entity = Album.class, parentColumns = "id", childColumns = "album_id"),
    @ForeignKey(entity = Song.class, parentColumns = "id", childColumns = "song_id")
})
public class AlbumSong implements Serializable {

  @PrimaryKey(autoGenerate = true)
  @ColumnInfo(name = "id")
  @SerializedName("id")
  private int mId;
  @SerializedName("album_id")
  @ColumnInfo(name = "album_id")
  private int mAlbumId;
  @SerializedName("song_id")
  @ColumnInfo(name = "song_id")
  private int mSongId;


  public AlbumSong(int id, int albumId, int songId) {
    mId = id;
    mAlbumId = albumId;
    mSongId = songId;
  }

  public int getId() {
    return mId;
  }

  public void setId(int id) {
    mId = id;
  }

  public int getAlbumId() {
    return mAlbumId;
  }

  public void setAlbumId(int albumId) {
    mAlbumId = albumId;
  }

  public int getSongId() {
    return mSongId;
  }

  public void setSongId(int songId) {
    mSongId = songId;
  }
}
