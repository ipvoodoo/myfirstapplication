package ru.echodc.myfirstapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.echodc.myfirstapplication.albums.AlbumsActivity;

public class AuthFragment extends Fragment {

  public static final String TAG = AuthFragment.class.getSimpleName();

  @BindView(R.id.etLogin)
  AutoCompleteTextView mEmail;
  @BindView(R.id.etPassword)
  EditText mPassword;
  @BindView(R.id.btnEnter)
  Button btnEnter;
  @BindView(R.id.btnRegister)
  Button btnRegister;
  @BindView(R.id.cvAuthorisation)
  CardView mCvAuthorisation;

  private Errors mError;
  private Animation mShake;

  public static AuthFragment newInstance() {
    Bundle args = new Bundle();

    AuthFragment fragment = new AuthFragment();
    fragment.setArguments(args);
    return fragment;
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

    View v = inflater.inflate(R.layout.fr_auth, container, false);

    ButterKnife.bind(this, v);

    initErrorAnimation();
    return v;
  }

  // region ===================== CheckValidation =====================
  private boolean isEmailValid() {
    return !TextUtils.isEmpty(mEmail.getText())
        && Patterns.EMAIL_ADDRESS.matcher(mEmail.getText()).matches();
  }

  private boolean isPasswordValid() {
    return !TextUtils.isEmpty(mPassword.getText());
  }
  // endregion CheckValidation

  // region ===================== Others =====================
  private void showMessage(@StringRes int string) {
    Toast.makeText(getActivity(), string, Toast.LENGTH_LONG).show();
  }
  // endregion Others

  // region ===================== Events =====================
  @OnClick(R.id.btnEnter)
  public void buttonEnterClick() {
    if (isEmailValid() && isPasswordValid()) {

      ApiUtils.getBasicAuthClient(mEmail.getText().toString(), mPassword.getText().toString(), true);

      Disposable disposable = ApiUtils.getApiService().authorization()
          .subscribeOn(Schedulers.io())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe(
              user -> {
                startActivity(new Intent(getActivity(), AlbumsActivity.class));
                getActivity().finish();
              }, throwable -> {
                if (ApiUtils.NETWORK_EXCEPTIONS.contains(throwable.getClass())) {
                  Log.d(TAG, "getAlbums: " + " Нет сети!");
                  showMessage(R.string.error_network_error);
                }else {
                  throwable.printStackTrace();
                  showMessage(R.string.auth_error);
                }
              });

    } else {
      showMessage(R.string.input_error);
      mCvAuthorisation.startAnimation(mShake);
    }
  }

  @OnClick(R.id.btnRegister)
  public void buttonRegisterClick() {
    getFragmentManager()
        .beginTransaction()
        .replace(R.id.fragmentContainer, RegistrationFragment.newInstance())
        .addToBackStack(RegistrationFragment.class.getName())
        .commit();
  }

  //  @OnFocusChange(R.id.etLogin)
  //  public void emailFocusChanged(View view, boolean hasFocus) {
  //    if (hasFocus) {
  //      mEmail.showDropDown();
  //    }
  //  }
  // endregion Events

  // region ===================== Animation =====================
  private void initErrorAnimation() {
    mShake = new AnimationUtils().loadAnimation(getActivity(), R.anim.shake);
  }
  // endregion Animation

  // region ===================== Errors =====================

  private void showError(int code) {
    @StringRes int message;
    switch (code) {
      case 400:
        message = R.string.response_code_400;
        break;
      case 204:
        message = R.string.response_code_204;
        break;
      case 404:
        message = R.string.response_code_404;
        break;
      case 401:
        message = R.string.auth_error;
        break;
      case 500:
        message = R.string.response_code_500;
        break;
      default:
        message = R.string.registration_error;
    }
    showMessage(message);
  }
  // endregion Errors

}
