package ru.echodc.myfirstapplication;

import io.reactivex.Completable;
import io.reactivex.Single;
import java.util.List;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import ru.echodc.myfirstapplication.model.Album;
import ru.echodc.myfirstapplication.model.Comment;
import ru.echodc.myfirstapplication.model.Song;
import ru.echodc.myfirstapplication.model.User;

public interface AcademyApi {

  //  Void -  возвращет только серверные ошибки
  @POST("registration")
  Completable registration(@Body User user);

  @GET("albums")
  Single<List<Album>> getAlbums();

  @GET("albums/{id}")
  Single<Album> getAlbum(@Path("id") int id);

  @GET("songs")
  Single<List<Song>> getSongs();

  @GET("songs/{id}")
  Single<Song> getSong(@Path("id") int id);

  @GET("user")
    //  Call<User> authorization(@Path("email") String email, @Path("password") String password);
    //  Call<User> authorization(@Header("Authorization") String credentials);
  Single<User> authorization();

  @GET("comments")
  Single<List<Comment>> getCommentsFromServer();

  @GET("albums/{id}/comments")
  Single<List<Comment>> getCommentsFromServerByAlbumId(@Path("id") int id);

  @GET("comments/{id}")
  Single<Comment> getCommentByIdFromServer(@Path("id") int id);

  @POST("comments")
  Completable sendCommentToServer(@Body Comment comment);


}
