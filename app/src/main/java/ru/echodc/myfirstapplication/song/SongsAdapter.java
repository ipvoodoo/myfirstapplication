package ru.echodc.myfirstapplication.song;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import ru.echodc.myfirstapplication.R;
import ru.echodc.myfirstapplication.model.Song;

public class SongsAdapter extends RecyclerView.Adapter<SongsHolder> {

  @NonNull
  private final List<Song> mSongs = new ArrayList<>();
  private final OnItemClickListener mOnClickListener;

  public SongsAdapter(OnItemClickListener onClickListener) {
    mOnClickListener = onClickListener;
  }

  @Override
  public SongsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    View view = inflater.inflate(R.layout.list_item_song, parent, false);
    return new SongsHolder(view);
  }

  @Override
  public void onBindViewHolder(SongsHolder holder, int position) {
    Song song = mSongs.get(position);
    holder.bind(song, mOnClickListener);
  }

  @Override
  public int getItemCount() {
    return mSongs.size();
  }

  public void addData(List<Song> data, boolean isRefreshed) {
    if (isRefreshed) {
      mSongs.clear();
    }

    mSongs.addAll(data);
    //    mSongs.add((Song) data.get(0));
    sortSongsById(data);
    notifyDataSetChanged();
  }

  public interface OnItemClickListener {

    void onItemClick(Song song);
  }

  private void sortSongsById(List<Song> songs) {
    Collections.sort(songs, (l1, l2) -> {
      if (l1.getId() > l2.getId()) {
        return 1;
      } else if (l1.getId() < l2.getId()) {
        return -1;
      } else {
        return 0;
      }
    });
  }
}
