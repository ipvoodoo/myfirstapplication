package ru.echodc.myfirstapplication.song;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import pl.bclogic.pulsator4droid.library.PulsatorLayout;
import ru.echodc.myfirstapplication.ApiUtils;
import ru.echodc.myfirstapplication.App;
import ru.echodc.myfirstapplication.R;
import ru.echodc.myfirstapplication.db.MusicDao;
import ru.echodc.myfirstapplication.model.Song;

public class SongFragment extends Fragment {

  private static final String SONG_KEY = "SONG_KEY";
  private static final String TAG = SongFragment.class.getSimpleName();

  @BindView(R.id.tv_song_name)
  TextView mTvSongName;
  @BindView(R.id.tv_song_duration)
  TextView mTvSongDuration;
  @BindView(R.id.iv_song_image)
  ImageView mIvSongImage;
  @BindView(R.id.pulsator)
  PulsatorLayout mPulsator;
  //  @BindView(R.id.errorView)
  //  View mErrorView;

  private Animation mNoteAnim;

  private Song mSong;

  public static SongFragment newInstance(Song song) {
    Bundle args = new Bundle();
    args.putSerializable(SONG_KEY, song);

    SongFragment fragment = new SongFragment();
    fragment.setArguments(args);

    return fragment;
  }

  //  public static SongFragment newInstance() {
  //    return new SongFragment();
  //  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fr_song, container, false);

    return v;
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    ButterKnife.bind(this, view);
    initAnimation();
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    mSong = (Song) getArguments().getSerializable(SONG_KEY);
    getActivity().setTitle(mSong.getName());
    getSong();
  }

  private void getSong() {
    ApiUtils.getApiService()
        .getSong(mSong.getId())
        .subscribeOn(Schedulers.io())
        .doOnSuccess(song -> getMusicDao().insertSong(song))
        .onErrorReturn(throwable -> {
          if (ApiUtils.NETWORK_EXCEPTIONS.contains(throwable.getClass())) {
            Log.d(TAG, "getSong: " + " Нет сети!");
            return getMusicDao().getSong();
          } else {
            return null;
          }
        })
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(song -> {
          mTvSongName.setText(mSong.getName());
          mTvSongDuration.setText(mSong.getDuration());
          mPulsator.start();
          mIvSongImage.startAnimation(mNoteAnim);
          //              mErrorView.setVisibility(View.GONE);
        });
  }

  private MusicDao getMusicDao() {
    return ((App) getActivity().getApplication()).getDatabase().getMusicDao();
  }

  private void initAnimation() {
    mNoteAnim = new AnimationUtils().loadAnimation(getActivity(), R.anim.music_note_anim);
  }
}
