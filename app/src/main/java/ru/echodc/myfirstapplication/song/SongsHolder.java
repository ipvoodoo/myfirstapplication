package ru.echodc.myfirstapplication.song;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.echodc.myfirstapplication.R;
import ru.echodc.myfirstapplication.model.Song;


public class SongsHolder extends RecyclerView.ViewHolder {

  @BindView(R.id.tv_title)
  TextView mTitle;
  @BindView(R.id.tv_duration)
  TextView mDuration;
  @BindView(R.id.tv_id)
  TextView mTvId;

  public SongsHolder(View itemView) {
    super(itemView);
    ButterKnife.bind(this, itemView);
  }

  public void bind(Song item, SongsAdapter.OnItemClickListener onItemClickListener) {
    mTitle.setText(item.getName());
    mDuration.setText(item.getDuration());
    mTvId.setText(String.valueOf(item.getId()));
    if (onItemClickListener != null) {
      itemView.setOnClickListener(view -> onItemClickListener.onItemClick(item));
    }
  }
}
