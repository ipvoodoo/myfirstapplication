package ru.echodc.myfirstapplication;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class Errors {

  @SerializedName("email")
  private List<String> email;
  @SerializedName("name")
  private List<String> name;
  @SerializedName("password")
  private List<String> password;
  @SerializedName("errors")
  private Errors errors;

  private int id;

  public int getId() {
    return id;
  }

  public List<String> getEmail() {
    return email;
  }

  public void setEmail(List<String> email) {
    this.email = email;
  }

  public List<String> getName() {
    return name;
  }

  public void setName(List<String> name) {
    this.name = name;
  }

  public List<String> getPassword() {
    return password;
  }

  public void setPassword(List<String> password) {
    this.password = password;
  }

  public Errors getErrors() {
    return errors;
  }

  public void setErrors(Errors errors) {
    this.errors = errors;
  }

}
