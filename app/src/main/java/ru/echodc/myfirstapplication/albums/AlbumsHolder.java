package ru.echodc.myfirstapplication.albums;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.echodc.myfirstapplication.R;
import ru.echodc.myfirstapplication.model.Album;

public class AlbumsHolder extends RecyclerView.ViewHolder {

  @BindView(R.id.tv_title)
  TextView mTitle;
  @BindView(R.id.tv_release_date)
  TextView mReleaseDate;

  public AlbumsHolder(View itemView) {
    super(itemView);
    ButterKnife.bind(this,itemView);
  }

  public void bind(Album item, AlbumsAdapter.OnItemClickListener onItemClickListener) {
    mTitle.setText(item.getName());
    mReleaseDate.setText(item.getReleaseDate());
    if (onItemClickListener != null) {
      itemView.setOnClickListener(v -> onItemClickListener.onItemClick(item));
    }
  }
}
