package ru.echodc.myfirstapplication.albums;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.echodc.myfirstapplication.ApiUtils;
import ru.echodc.myfirstapplication.App;
import ru.echodc.myfirstapplication.NetworkStatusChecker;
import ru.echodc.myfirstapplication.R;
import ru.echodc.myfirstapplication.album.DetailAlbumFragment;
import ru.echodc.myfirstapplication.db.MusicDao;

public class AlbumsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

  private static final String TAG = AlbumsFragment.class.getSimpleName();

  @BindView(R.id.recycler)
  RecyclerView mRecyclerView;
  @BindView(R.id.refresher)
  SwipeRefreshLayout mRefresher;
  @BindView(R.id.errorView)
  View mErrorView;

  //  Click on item in adapter
  @NonNull
  private final AlbumsAdapter mAlbumAdapter = new AlbumsAdapter(
      album -> {
        getFragmentManager().beginTransaction()
            .replace(R.id.fragmentContainer, DetailAlbumFragment.newInstance(album))
            .addToBackStack(DetailAlbumFragment.class.getSimpleName())
            .commit();
      });

  public static AlbumsFragment newInstance() {
    return new AlbumsFragment();
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

    return inflater.inflate(R.layout.fr_recycler, container, false);
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    ButterKnife.bind(this, view);
    mRefresher.setOnRefreshListener(this);
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    getActivity().setTitle(R.string.albums);
    mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    mRecyclerView.setAdapter(mAlbumAdapter);

    onRefresh();
  }

  @Override
  public void onRefresh() {
    mRefresher.post(this::getAlbums);
  }

  @SuppressLint("CheckResult")
  private void getAlbums() {
    ApiUtils.getApiService()
        .getAlbums()
        .subscribeOn(Schedulers.io())
        .doOnSuccess(albums -> getMusicDao().insertAlbums(albums))
        .onErrorReturn(throwable -> {
          if(!NetworkStatusChecker.isNetworkAviable(getActivity())){
            Log.d(TAG, "getAlbums: " + " Нет сети!");
            return getMusicDao().getAlbums();
          }else {
            return null;
          }

//          if (ApiUtils.NETWORK_EXCEPTIONS.contains(throwable.getClass())) {
//            Log.d(TAG, "getAlbums: " + " Нет сети!");
//            return getMusicDao().getAlbums();
//          } else {
//            return null;
//          }
        })
        .observeOn(AndroidSchedulers.mainThread())
        .doOnSubscribe(disposable -> mRefresher.setRefreshing(true))
        .doFinally(() -> mRefresher.setRefreshing(false))
        .subscribe(
            albums -> {
              mErrorView.setVisibility(View.GONE);
              mRecyclerView.setVisibility(View.VISIBLE);
              mAlbumAdapter.addData(albums, true);
            }, throwable -> {
              mErrorView.setVisibility(View.VISIBLE);
              mRecyclerView.setVisibility(View.GONE);
            });
  }

  private MusicDao getMusicDao() {
    return ((App) getActivity().getApplication()).getDatabase().getMusicDao();
  }
}
