package ru.echodc.myfirstapplication.db;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import ru.echodc.myfirstapplication.model.Album;
import ru.echodc.myfirstapplication.model.AlbumSong;
import ru.echodc.myfirstapplication.model.Comment;
import ru.echodc.myfirstapplication.model.Song;
import ru.echodc.myfirstapplication.model.converter.AlbumConverter;

@Database(entities = {Album.class, Song.class, AlbumSong.class, Comment.class}, version = 1)
@TypeConverters(AlbumConverter.class)
public abstract class DataBase extends RoomDatabase {

  public abstract MusicDao getMusicDao();
}
