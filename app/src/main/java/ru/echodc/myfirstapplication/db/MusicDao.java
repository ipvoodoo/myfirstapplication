package ru.echodc.myfirstapplication.db;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import java.util.List;
import ru.echodc.myfirstapplication.model.Album;
import ru.echodc.myfirstapplication.model.AlbumSong;
import ru.echodc.myfirstapplication.model.Comment;
import ru.echodc.myfirstapplication.model.Song;

@Dao // интерфейс для работы с таблицами
public interface MusicDao {

  // region ===================== Album =====================
  @Insert(onConflict = OnConflictStrategy.REPLACE)
  void insertAlbums(List<Album> albums);

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  long insertAlbum(Album album);

  @Query("select * from album")
  List<Album> getAlbums();

  @Query("select * from album where id =:albumId")
  Album getAlbumWithId(int albumId);

  //удалить альбом по id
  @Query("DELETE FROM album where id = :albumId")
  int deleteAlbumById(int albumId);

  @Delete
  void deleteAlbum(Album album);
  // endregion Album

  // region ===================== Song =====================
  @Insert(onConflict = OnConflictStrategy.REPLACE)
  void insertSongs(List<Song> songs);

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  long insertSong(Song song);

  @Query("select * from song")
  List<Song> getSongs();

  @Query("select * from song")
  Song getSong();

  @Update
  int updateSong(Song song);

  @Update
  int updateAlbumSong(AlbumSong albumSong);

  @Query("DELETE FROM song where id = :songId")
  int deleteSongById(int songId);

  // endregion Song

  // region ===================== AlbumSong =====================
  @Insert(onConflict = OnConflictStrategy.REPLACE)
  void setLinksAlbumSongs(List<AlbumSong> linksAlbumSongs);

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  long setLinkAlbumSong(AlbumSong albumSongs);

  @Query("select * from albumsong")
  List<AlbumSong> getAlbumSongs();

  //получить список песен переданного id альбома
//  @Query("select * from song inner join albumsong on song.id = albumsong.song_id where album_id = :albumId")
  @Query("SELECT * FROM song sn, albumsong asg WHERE sn.id = asg.song_id and asg.album_id = :albumId")
  List<Song> getSongsFromAlbum(int albumId);
  // endregion AlbumSong

  // region ===================== Comment =====================
  @Insert(onConflict = OnConflictStrategy.REPLACE)
  void insertComments(List<Comment> comments);

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  long insertComment(Comment comment);

  @Query("select * from comment ORDER BY comment.id")
  List<Comment> getComments();

  @Query("SELECT * FROM comment WHERE album_id = :albumId ORDER BY comment.id")
  List<Comment> getAlbumComments(int albumId);


  @Query("DELETE FROM comment where id = :commentId")
  int deleteCommentById(int commentId);
  // endregion Comment

  //обновить информацию об альбоме
//  @Update
//  int updateAlbum(Album album);
//
//  @Update
//  int updateComment(Comment comment);
//
//
//  @Query("DELETE FROM albumsong where album_id = :albumId or song_id = :songId")
//  void deleteAlbumSongByAlbumIdOrSongId(Integer albumId, Integer songId);
//
//  @Query("DELETE FROM albumsong where id = :albumSongId")
//  int deleteAlbumSongById(int albumSongId);



}
